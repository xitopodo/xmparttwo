import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.Dimension;
import org.junit.*;
import org.openqa.selenium.JavascriptExecutor;
import java.util.List;
import java.util.ArrayList;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.NoSuchElementException;

public class CompareData {

    public static void main(String[] args) throws InterruptedException {

        // Chrome browser setup
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\pc\\Downloads\\Selenium Course\\chromedriver_win32\\chromedriver.exe");
        WebDriver chromeDriver = new ChromeDriver();
        
        // Run the test with maximum screen resolution
        chromeDriver.manage().window().maximize();
        runTest(chromeDriver);

        // Run the test with 1024x768 resolution
        chromeDriver.manage().window().setSize(new Dimension(1024, 768));
        runTest(chromeDriver, true, false); // Pass true to indicate 1024x768 resolution

        // Run the test with 800x600 resolution
        chromeDriver.manage().window().setSize(new Dimension(800, 600));
        runTest(chromeDriver, false, true); // Pass false to indicate 800x600 resolution

        chromeDriver.quit();
    }

    public static void runTest(WebDriver driver) throws InterruptedException {
        runTest(driver, false, false); // Default resolution (not 1024x768)
    }

    public static void runTest(WebDriver driver, boolean is1024x768, boolean is800x600) throws InterruptedException {
        driver.get("https://www.xm.com/");
        // Click on "Accept All" on privacy if visible
        try {
            WebElement acceptCookieButton = driver.findElement(By.xpath("//button[@class='btn btn-block btn-red btn-solid js-acceptDefaultCookie gtm-acceptDefaultCookieFirstVisit']"));
            if (acceptCookieButton.isDisplayed()) {
                acceptCookieButton.click();
            }
        } catch (NoSuchElementException | TimeoutException e) {
            // Element not found or not clickable
            System.out.println("Accept cookie button not found or not clickable. Proceeding with next steps.");
        }

        WebElement openAccountBanner = driver.findElement(By.xpath("//div[@class='block']//div[contains(text(),'Open an Account')]"));
        Assert.assertTrue(openAccountBanner.isDisplayed());
        
        if (is800x600) {
       	 	driver.findElement(By.xpath("//button[@class='toggleLeftNav']//span")).click();
       	 	Thread.sleep(2000);
       	 	driver.findElement(By.xpath("//a[@href='#tradingMenu']")).click();
       	 	Thread.sleep(2000);
       	 	driver.findElement(By.xpath("//span[normalize-space()='Stocks']")).click();
       	 	Thread.sleep(5000);
        } else {
            driver.findElement(By.xpath("//a[normalize-space()='Trading']")).click();
            String tradingText = driver.findElement(By.xpath("//li[@class='main_nav_trading selected']//div[@class='box hidden-xs hidden-sm']//p[1]")).getText();
            Assert.assertEquals(tradingText, "At XM we offer Ultra Low Micro and Ultra Low Standard Accounts that can match the needs of novice and experienced traders with flexible trading conditions.");
            driver.findElement(By.xpath("//li[@class='menu-stocks']//a[normalize-space()='Stocks']")).click();
            Thread.sleep(5000); //Added some wait to load website - 5000 milliseconds = 5 seconds
        }

        String stocks = driver.findElement(By.xpath("//h2[normalize-space()='Advantages of Stock Trading at XM']")).getText();
        Assert.assertEquals(stocks, "Advantages of Stock Trading at XM");

        WebElement norwayFilter = driver.findElement(By.xpath("//button[@data-value='Norway']"));
        WebElement searchBar = driver.findElement(By.xpath("//div[@class='tab-pane active']//input[@type='search']"));
        Actions actions = new Actions(driver);
        actions.moveToElement(searchBar).perform(); //Scroll until Norway is visible
        norwayFilter.click();

        WebElement columnsTable = driver.findElement(By.xpath("//th[@aria-label='Symbol with Description: activate to sort column ascending']"));
        actions.moveToElement(columnsTable).perform(); //Scroll until Search Bar is visible

        searchBar.click();
        searchBar.sendKeys("Orkla ASA (ORK.OL)");
        
        WebElement footer = driver.findElement(By.xpath("//p[contains(text(),'* Swap rates are calculated according to the stock')]"));
        actions.moveToElement(footer).perform(); //Scroll until Read More is visible
        
        Thread.sleep(3000); //Added some wait to load website - 3000 milliseconds = 3 seconds
        
        String symbol = driver.findElement(By.xpath("//td[normalize-space()='Orkla']")).getText();
        String spread = driver.findElement(By.xpath("//td[@data-xm-qa-name='minSpread']")).getText();
        String tradeSize = driver.findElement(By.xpath("//td[@data-xm-qa-name='minMaxTradeSize']")).getText();
        String marginPercentage = driver.findElement(By.xpath("//td[@data-xm-qa-name='marginRequirement']")).getText();
        String longSwap = driver.findElement(By.xpath("//td[@data-xm-qa-name='swapLong']")).getText();
        String shortSwap = driver.findElement(By.xpath("//td[@data-xm-qa-name='swapShort']")).getText();
        String limitAndStopLevels = driver.findElement(By.xpath("//td[@data-xm-qa-name='limitStopLevel']")).getText();
        
        if (is1024x768 || is800x600) {
            driver.findElement(By.xpath("//td[@data-xm-qa-name='symbolWithDescription']")).click();
            driver.findElement(By.xpath("//span[@class='dtr-data']//a[@class='btn btn-green']")).click();
            Thread.sleep(10000); //Added some wait to load website - 10 seconds
        } else {
            driver.findElement(By.xpath("//a[@class='btn btn-green']")).click();
            Thread.sleep(10000); //Added some wait to load website - 10 seconds
        }
        
        WebElement tradingConditions = driver.findElement(By.xpath("//i[@class='fa fa-clock-o']"));
        actions.moveToElement(tradingConditions).perform(); //Scroll until Trading Conditions are visible
        Thread.sleep(3000); //Added some wait to load website - 3000 milliseconds = 3 seconds

        
        String symbolTrading = driver.findElement(By.xpath("//span[normalize-space()='Orkla']")).getText();
        String spreadTrading = driver.findElement(By.xpath("//tr[@class='hidden-xs']//td[@class='ltr']//strong")).getText();
        String tradeSizeTrading = driver.findElement(By.xpath("//td[@data-xm-qa-name='min_max_trade_size__value']")).getText();
        String marginPercentageTrading = driver.findElement(By.xpath("//td[@data-xm-qa-name='margin_requirement__value']//strong")).getText();
        String longSwapTrading = driver.findElement(By.xpath("//td[@data-xm-qa-name='swap_value_in_margin_currency_long__value']")).getText();
        String shortSwapTrading = driver.findElement(By.xpath("//td[@data-xm-qa-name='swap_value_in_margin_currency_short__value']//span[@class='red']")).getText();
        String limitAndStopLevelsTrading = driver.findElement(By.xpath("//td[@data-xm-qa-name='limit_and_stop_levels__title']//span[@class='red']")).getText();
        
        Assert.assertEquals(symbol, symbolTrading);
        Assert.assertEquals(spread, spreadTrading);
        Assert.assertEquals(tradeSize, tradeSizeTrading);
        Assert.assertEquals(marginPercentage, marginPercentageTrading);
        Assert.assertEquals(longSwap, longSwapTrading);
        Assert.assertEquals(shortSwap, shortSwapTrading);
        Assert.assertEquals(limitAndStopLevels, limitAndStopLevelsTrading);
        
    }
}
